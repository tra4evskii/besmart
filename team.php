<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Team</title>

<?php  echo HTML::script('app/assets/js/pace.min.js')?>
<?php  echo HTML::style('/app/assets/css/pace.css')?>
<?php  echo HTML::style('/app/assets/css/main.css')?>
<?php  echo HTML::style('/app/assets/css/partcss.css')?>
<?php  echo HTML::script('app/assets/js/jquery.js')?>
<?php echo HTML::style('app/assets/css/chosen.css')?>
<?php echo HTML::style('app/assets/css/jquery.dataTables.css')?>
<?php  echo HTML::script('app/assets/js/chosen.jquery.min.js')?>
<?php  echo HTML::script('app/assets/js/jquery.dataTables.min.js')?>
<?php echo HTML::style('app/assets/css/rateit.css')?>
<?php  echo HTML::script('app/assets/js/jquery.rateit.min.js')?>
<?php  echo HTML::script('app/assets/js/feedback.js')?>
<?php  echo HTML::script('app/assets/js/jquery.validate.js')?>
<?php  echo HTML::script('app/assets/js/jquery.jeditable.js')?>
<?php  echo HTML::script('app/assets/js/jquery.dataTables.editable.js')?>
<?php  echo HTML::script('app/assets/js/headerbar.js')?>
<?php  echo HTML::script('app/assets/js/jquery.session.js')?>

<script>
function ico(X){
 $('#TileView').css("background-color","#ffffff");
  $('#ListView').css("background-color","#ffffff");
   $('#law').css("background-color","#ffffff");
   $(X).css("background-color","#d9dee0");
   }
</script>

</head>

	<body >
	    <script>
    var tile = function(){
        $('#PeopleTile').show();
        $('#PeopleList').hide();
        $('#AccessTable').hide();
		$('#chosenBlock').show();
		
		
			var z = 0;
		
			$('li[class="Contact"]').each(function() {
			 z = z + 1;
				if(z >10){
					$(this).css("display","none");
				}
		
			})
			
	}
      var list = function(){
        $('#PeopleTile').hide();
        $('#PeopleList').show();
        $('#AccessTable').hide();

        $('#chosenBlock').show();
      }
	  
      var access = function(){
        $('#PeopleTile').hide();
        $('#PeopleList').hide();
        $('#AccessTable').show();

        $('#chosenBlock').hide();
      }
      </script>
	  
<?php 
echo $mainheader;
?>
		

<div id="container">	
	<div id="SearchBox" >  
        <div class="Container" style="height:50px">
                <div class="Caption">Показать по </div> 
                <div class="Option">
                    <select id="paginationSizeSelect">
                        <option>10</option>
                        <option>25</option>
                        <option>50</option>
                        <option>100</option>
                    </select> 
                </div>
                <a href="javascript:tile()" id="TileView" onclick="ico('#TileView')"></a>
                <a href="javascript:list()" id="ListView" onclick="ico('#ListView')"></a>
                <?php  if(Auth::user()->admin==1){?>
                <a href="javascript:access()"  id="law" onclick="ico('#law')"></a>
                <?php }?>
                <div class="SearchLine">
                  <input id="searchInput"/>
                  <a href=""></a>
                </div>
                
        </div>        
    </div>
	<div>
        <div  id="chosenBlock">
            <select data-placeholder="Choose a Country..." class="chosen-select" multiple  id="tagsSelect" tabindex="4">
			
			
				<option value=""></option> 
				<?php 
				$project_id=Input::get("project_id");
				foreach (Project::order_By('name','ASC')->get() as $project) {
					if( $project_id!=null){
							if($project_id==$project->id){
								echo '<option value="'.$project->id.'" selected>'.$project->name.'</option>';
							}else{
								echo '<option value="'.$project->id.'">'.$project->name.'</option>';
							}
					}else {
				  echo '<option value="'.$project->id.'" selected>'.$project->name.'</option>';
				  }
				}
				?>
            
         
          </select> 
        </div>
    </div>
        

  <div id="PeopleTile">
          <ul class="Container">
              
 
 <?php 
$cur_user =  Auth::user()->id;
$rows1 = \Project\User::where('user_id','=',$cur_user)->first();
if (isset($rows1) AND $rows1->role_id == 1){
		$show = 1 ;//Admin
}
$rows = \Project\User::where('user_id','=',$cur_user)->where('role_id','=',2)->get();
foreach ($rows as $row)

	{
	$role =  $row->role_id;
	$project_id = $row->project_id;
		
		if ($role == 2){ //СЕО
		
		$workers = \Project\User::where('project_id','=',$project_id)->where('role_id','!=',2)->get();
			foreach($workers as $worker){
			$arr[] = $worker->user_id;
			
			}
		
		}
		
		
	}
 foreach (User::order_by('lastname','ASC')->get() as $item) {
	if($item->deleted)
	{
		continue;
	}
	
	$show = 0 ;	
	if(!empty($arr)){
	
		if(in_array($item->id, $arr)) {
		$show = 1;
		}
	}
  if(Auth::user()->admin==1){
    $show = 1;
  }
	

  $linkedProjects = \Project\User::where('user_id','=',$item->id)->get(array('project_id'));
  $linkedProjectIds =  array();
  foreach ($linkedProjects as $project) {
    $linkedProjectIds[] = $project->project_id;
  }

echo '<li class="Contact" data-projects=\''.json_encode($linkedProjectIds ).'\' user_id = "'. $item->id .'"> 
                  <div class="Header">
                      <div class="Photo">';
echo (empty($item->avatar_name))?'<div class="photoImage" ></div>':'<img src="'.URL::to('../upload/avatars/thumbs/'.$item->avatar_name).'" width=60 height=60 class="photoImage" >';
                      echo '</div>
                        <div class="Information">
                          <h3><a class="linkforteammember" user_id="'. $item->id .'"  href="'.(URL::to('teammember/'.$item->id.'/membersinfo')).'">'.$item->lastname." ".$item->firstname.'</a></h3>
						  <h3>';
						  if(isset($item->post))
								{
								echo $item->post;
								}
						  echo '</h3>
                            <h4 >'.$item->position.'</h4>';
                            echo (empty($item->company)?' <div  ></div>':'<a class="link" href=""  >'.$item->company.'</a>');
                            
                         echo   '<ul class="Stars">
                                 <div cur_user_id="'.$item->id.'" class="rateit bigstars" data-rateit-starwidth="14" data-rateit-starheight="13" data-rateit-resetable="false" super="rateUsr"
								data-rateit-value="';
								$count = User\Rate::where('cur_user_id','=',$item->id)->count();
								if ($count != 0)
								{
									$results = User\Rate::where('cur_user_id','=',$item->id)->get();
									$res = 0;
									foreach ($results as $r)
									{
										$res = $res + $r->rate;
									}
									echo $res/$count;
								}
								
							echo	'" ></div>
                          </ul>
                            <br /><br />
                            <div class="Contacts">
                              <span class="Caption" '.((empty($item->phones))?'style="visibility:hidden"':'').'>Моб.:</span>
                                <span class="Value"><a href="skype:'.$item->phones.'">'.$item->phones.'</a></span>
                            </div>
              <div class="Contacts">
                              <span class="Caption" '.((empty($item->email))?'style="visibility:hidden"':'').'>E-mail.:</span>
                                <span class="Value" ><a href="mailto:'.$item->email.'">'.$item->email.'</a></span>
                            </div>
                        </div>
                       
                    </div>
                    
                    <div class="Social">

                      <ul>
                          <li>Социальные сети:</li>';

                if(!empty($item->facebook)){
                    echo '<li class="fb"><a href="'. $item->facebook .'"></a></li>';
                  
                }
                if(!empty($item->linked_in)){
                   echo '<li class="in"><a href="'. $item->linked_in .'"></a></li>';
                 }
                  if(!empty($item->vk)){
                      echo '<li class="vk"><a href="'. $item->vk .'"></a></li>';
                    }
                     if(!empty($item->skype)){
                       echo '<li class="sk"><a href="skype://'.$item->skype.'"></a></li>';
                     }
				echo '
				</ul>'; 
				if (isset($show) AND $show == 1){
					echo '<div>
                    <div class="Actions">
                      <a href="'.(URL::to('teammember/'.$item->id.'/edit')).'" class="Edit">Редактировать</a>
                        <a href="'.(URL::to('teammember/'.$item->id.'/remove')).'" class="Delete">Удалить</a>
                    </div>';
						
				}
					echo '</li>';
					
  }
              ?>  
                

            </ul>
        </div>
		
        <div id="PeopleList"  style="display:none" class="qrushDataTable">
		
            <table id="table">
            	         	<thead>
				<tr>
				<th width=80>Фото</th>
                <th width=120><p>ФИО</p><div class="sort"></div></th>
                <th><p>Должность</p> <div class="sort"></div></th>
				<th width=120><p>Телефон</p> <div class="sort"></div></th>
				<th width=160><p>E-mail</p> <div class="sort"></div></th>
				<th width=140>Социальные сети</th>
			
				 <th width=103>Действия</th>
				
				</tr>
          	</thead>
          	<tbody>
          		<?php foreach (User::order_by('firstname','ASC')->get() as $item) 
				{
				if($item->deleted)
					{
						continue;
					}
				$show = 0;
				if(!empty($arr)){
  
					if(in_array($item->id, $arr)) {
					$show = 1;
					}
				}
				
				if(Auth::user()->admin==1){
					$show = 1;
				}
          			
				$avatar = ""; 
				
				if($item->avatar_name)
					{
						$avatar= '<img src="'. (URL::to('../upload/avatars/thumbs/'.$item->avatar_name)) .'" width=60 height=60 class="photoImage" onerror="this.style.display = \"none\"/>';
					} 
          		echo "<tr>
				
				<td>".$avatar."&nbsp;</td>
				<td><a class=\"linkforteammember\" user_id=\"". $item->id ."\" style=\"text-decoration:none;\" href=\"".(URL::to('teammember/'.$item->id.'/membersinfo'))."\">".$item->lastname."  ".$item->firstname."</a></td>
				<td>";
				if(isset($item->post))
								{
								echo $item->post;
								}
				
				
				echo "</td>
				<td class='Phone' >".$item->phones."</td>
				<td class='Email' >".$item->email."</td>";
				  echo '<td><div class="Social">
                            <ul>
                         
                            ';
                if(!empty($item->facebook)){
                    echo '<li class="fb"><a href="'. $item->facebook .'"></a></li>';
                  
                }
                if(!empty($item->linkedin)){
                   echo '<li class="in"><a href="'. $item->linkedin .'"></a></li>';
                 }
                  if(!empty($item->vk)){
                      echo '<li class="vk"><a href="'. $item->vk .'"></a></li>';
                    }
                     if(!empty($item->skype)){
                       echo '<li class="sk"><a href="skype://'.$item->skype.'"></a></li>';
                     }
                echo '</ul></div></td>';
				echo '<td class="Actions">';
				
				if(isset($show) AND $show == 1){
				
						echo '
                        <div class="Container">
                            <a href="'.(URL::to('teammember/'.$item->id.'/edit')).'" class="Edit"></a>
                            <a href="'.(URL::to('teammember/'.$item->id.'/remove')).'" class="Delete"></a>
                            
                        </div>';
						
                }
                    
           
                    echo '</td></tr>';
          		
				}
          		?> 
          	</tbody>
          </table>
	</div>

	<div id="AccessTable" style="display:none">
		<?php 

		$projects = Project::order_By('name','ASC')->get();
		?>
	<table id="example">
    <thead>
		<tr><th width=80>Фото</th><th width=110>ФИО</th><th width=103>Действия</th>				
		<?php foreach ($projects as $item) 
					{

						echo '<th width=80 id="c'.$item->id.'">'.$item->name.'</th>';
					}

		?>
		</tr>
    </thead>
    <tbody>
        <?php foreach (User::order_By('lastname','ASC')->get() as $item) {
				if($item->deleted)
					{
						continue;
					}
          			
				$avatar = ""; 
				
				if($item->avatar_name)
					{
						$avatar= '<img src="'. (URL::to('../upload/avatars/thumbs/'.$item->avatar_name)) .'" width=60 height=60 class="photoImage" onerror="this.style.display = \"none\"/>';
					} 
          		echo "<tr id='".$item->id."'>
				
					<td>".$avatar."&nbsp;</td>
					<td>".$item->lastname." ".$item->firstname."</td>";
					
					echo '<td class="Actions">
							<div class="Container">
								<a href="'.(URL::to('teammember/'.$item->id.'/edit')).'" class="Edit"></a>
								<a href="'.(URL::to('teammember/'.$item->id.'/remove')).'" class="Delete"></a>
								
							</div>
					</td>';

						
						foreach ($projects as $proj) {
							$user = Project\User::where('user_id','=',$item->id)->where('project_id','=',$proj->id);
								if($user->count()!=0){
									$role = $user->first()->role_id;
									if($role==2){

								echo '<td>Руководитель</td>';
								}else if($role==3){

								echo '<td>Исполнитель</td>';
								}else {
									echo '<td>Нет доступа</td>';
								
								}
							}
						}
									
									
				
				
				echo '</tr>';
									
				
				}
        ?> 
    </tbody>
    </table>
	</div>
</body>
		
            
         
      


  <script type="text/javascript">
 

  
  
  $('#searchInput').keyup(function(){
	  var cur_input = $(this).val();
	  var LCcur_input = cur_input.toLowerCase();
	  
	  if(cur_input.length == 0 ){
	  
	  
		$('li[class="Contact"]').each(function() {
		
		var pagination =  $('#paginationSizeSelect').val();
		var paginationINT = parseInt(pagination, 10);
		var z = 0;
		
		$('li.Contact').show();
				$('li.Contact').each(function(){
				
					 z = z + 1;
							if(z > paginationINT){
								$(this).css("display","none");
								
							}
				
				})
				
					
			
				})
	  } else {
		console.log(inc);
			if(inc == "peopletile"){
				
					$('a.linkforteammember').each(function() {
				
					var link = $(this).html();
					var user_id = $(this).attr('user_id');
					var LClink = link.toLowerCase();
					var result = LClink.indexOf(LCcur_input);
					if(result == -1){
					
					$('li[user_id="'+user_id+'"]').hide();
					
					}else {
					$('li[user_id="'+user_id+'"]').show();
					
					}
					console.log('cur_inp ' + LCcur_input);
					console.log('LClink ' + LClink);
					console.log(result);
				
				
					})
		  
		  
			}
		}
	
	
	
  })

  	var inc = $.session.get('incontainer');
	if(typeof inc != "undefined"){
		
		if(inc == "peopletile"){
			$('#PeopleTile').show();
			$('#TileView').css('background-color','#D9E0DE');
			$('#PeopleList').hide();
			$('#ListView').css('background-color','#ffffff');
			$('#AccessTable').hide();
			$('#law').css('background-color','#ffffff');
		
			var z = 0;
		
			$('li[class="Contact"]').each(function() {
			 z = z + 1;
				if(z >10){
					$(this).css("display","none");
				}
		
			})
		
		
		
		}else if (inc == "peoplelist"){
		
			$('#PeopleTile').hide();
			$('#TileView').css('background-color','#ffffff');
			$('#PeopleList').show();
			$('#ListView').css('background-color','#D9E0DE');
			$('#AccessTable').hide();
			$('#law').css('background-color','#ffffff');
		

		
		}else if (inc == "AccessTable"){
		
			$('#PeopleTile').hide();
			$('#TileView').css('background-color','#ffffff');
			$('#PeopleList').hide();
			$('#ListView').css('background-color','#ffffff');
			$('#AccessTable').show();
			$('#law').css('background-color','#D9E0DE');
		
		}
	} else {
	
		var z = 0;
		$('li[class="Contact"]').each(function() {
			 z = z + 1;
			if(z >10){
				$(this).css("display","none");
			}
		
		})
		
		
		
		
		}
	
	
	
	
	
	 
window.onbeforeunload = function (evt) {
	peopletile 	=   $('#PeopleTile').css("display");
    peoplelist 	=  $('#PeopleList').css("display");
    AccessTable =     $('#AccessTable').css("display");
	if(peopletile != "none"){
	
	var incontainer = "peopletile";
	
	}
	
	if(peoplelist != "none"){
	
	var incontainer = "peoplelist";
	
	}
	
	if(AccessTable != "none"){
	
	var incontainer = "AccessTable";
	
	}
	
	
	$.session.set('incontainer',incontainer);
	
	
	var cur_th = $('th[class="sorting_asc"][class="sorting_desc"]');
	if(typeof cur_th != "undefined"){
		var class_cur_th = cur_th.attr("class");
		return class_cur_th;
	}	
    
}
  
  
  $(function(){ 

    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }});



$(document).ready(function() {
    $('#table').dataTable( {
	
	"aoColumns": [
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false }
		]

	});
	
} );
  
  $(document).ready(function() {
  
	var oTable = $('#table').dataTable( );
	$('#paginationSizeSelect').change(function(){
	
		var amount = $(this).val();
	
		var z = 0;
		$('li[class="Contact"]').each(function() {
			 z = z + 1;
			if(z > parseInt(amount)){
				$(this).css("display","none");
			} else {
			
				$(this).css("display","table-row");
			}
		
		})
	
	
		var aSettings = aTable.fnSettings();
		aSettings._iDisplayLength = $(this).val();
		aTable.fnDraw();
	
	
	
	
		var oSettings = oTable.fnSettings();
		oSettings._iDisplayLength = $(this).val();
		oTable.fnDraw();
	
  });

$('#tagsSelect').change(function() {
	var a = $(this).val();
	  $('li.Contact').each(function() 
	  { 
		
		var ids=$(this).data('projects');
	   console.log($(a).filter(ids));
		if (  $(a).filter(ids).length==0) 
		  {
			$(this).hide();
		  } else {
		
		  $(this).show();
		  }
	  });

});


  var aTable= $('#example').dataTable().makeEditable({
                       	sUpdateURL: "<?php  echo URL::to('team/accesstable');?>",
                        sSuccessResponse:"IGNORE",
                       	"aoColumns": [
                    				null,
                    				null,
                    				null

                    				<?php 	foreach ($projects as $item) 
				{

					echo ",{
						sName: '".$item->id."',
                						indicator: 'Обновление...',
                						loadtext: 'Обновление...',
                						type: 'select',
                						onblur: 'submit',
                						data: \"{'-1':'Нет доступа','3':'Испольнитель','2':'Руководитель'}\"
                						
                					}";
				}


				?>
                    				
					]									
				});

				
				
				
$('#searchInput').keyup(function(){
     oTable.fnFilter( $(this).val() );
     aTable.fnFilter( $(this).val() );
});
} );


			$('[super="rateUsr"]').bind('rated reset', function (e) {
				var ri = $(this);
				var value = ri.rateit('value');
				var cur_user_id=ri.attr('cur_user_id');
				$.ajax
					({
					type: 'POST',
					URL: '<?php  echo URL::to('team/rate');?>',
					data: { 
					'rate': value,
					'cur_user_id':cur_user_id
					},
					success: function(msg)
						{
					ri.rateit('value',msg);
					console.log(msg);
						}
					});
			});


		$(document).ready(function () {

			$('div[class="userInfo"]').mouseover(function(){
				$('div[id ="smallmenu"]').show();
			})


			$('div[class="userinfoandsmallmenu"]').mouseleave(function(){
				$('div[id ="smallmenu"]').hide();
			})
		
			$('a[id="allread"]').click(function () {
				event.preventDefault();
				
				$.ajax({
					type: 'POST',
					URL: '<?php  echo URL::to('teammember/allread')?>' ,
					data: {
					'user_id':<?php  echo Auth::user()->id;?>
					},
					success:function() {
		   
						window.location.href = '<?php  echo URL::to('teammember/') . Auth::user()->id . '/edit' ; ?>';
		   
					 }
				});  
				
			})
		})


	$('th').click(function(){
	
		var curth = $('th[class="sorting_desc"],[class="sorting_asc"]');
		var name = curth.attr("aria-label");
 
	})
  
  </script>

</html>