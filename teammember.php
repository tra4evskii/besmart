<?php

class TeamMember_Controller extends Base_Controller {

	

	public function __construct()
	{
		parent::__construct();

		$this->filter('before', 'teammember');
	}



	public function get_membersinfo($user_id){
	
		return  View::make('team.info')->nest('mainheader','layouts.mainfooter',array('classh1'=>'Team','textinh1'=>'Team','linkforback'=>URL::to('team')));
	
	}
	
	

	public function get_edit(){
	
		return  View::make('team.edit')->nest('mainheader','layouts.mainfooter',array('classh1'=>'Team','textinh1'=>'Team','linkforback'=>URL::to('team')));

	}
        
    public function post_allread() {   
	
	
        $user_id = Input::get('user_id');
		$new_notifications = Notification::where('user_id','=',$user_id)->where('read','=',0)->get();
		
        foreach ($new_notifications as $new_notification){
                            
                            $arr = array(
                            'read'=>1);
                            $new_notification->fill($arr);
                            $new_notification->save();
                            
        } 
		
		
        return true;

	
	}
        
        
        public function get_notifications()
	{
            
            
                        $cur_user_id = Auth::user()->id;
                        $new_notifications = Notification::where('user_id','=',$cur_user_id)->where('read','=',0)->get();
                        foreach ($new_notifications as $new_notification){
                            
                            $arr = array(
                            'read'=>1,
							);
							
								$new_notification->fill($arr);
								$new_notification->save();
                            
                        }
						
			return  View::make('team.notifications')->nest('mainheader','layouts.mainfooter',array('classh1'=>'Team','textinh1'=>'Notifications'));

	}
        
        

		
	public function post_photo(){
	
	
	
	$filename = null;
		
		if(isset($_FILES['photo'])){
			$userfile_name = $_FILES["photo"]["name"];
			$userfile_extn = substr($userfile_name, strrpos($userfile_name, '.')+1);
			$length = 10;
			$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
			$filename=$randomString.".".$userfile_extn;
			
			$path = path('app').'../../upload/avatars/';
		
		
			$file = \Input::file("photo");
			\File::upload("photo", $file_path = $path . $filename);
			 
			 
			 
			
			 
		 	 if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($path.$filename);
		} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($path.$filename);
		} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($path.$filename);
		} else if (preg_match('/[.](jpeg)$/', $filename)) {
		$im = imagecreatefromjpeg($path.$filename);
		}
		
		
			if(isset($im) AND ($im != 0)) 
			 {
				$ox = imagesx($im);
				$oy = imagesy($im);
				
					if(($ox>300) OR ($oy>300)){
								if($ox > $oy)
								{
								$di = ($ox - $oy);
								$dif = $di/2;
								
								
								$nm = imagecreatetruecolor($oy, $oy);
								imagecopy($nm, $im, 0,0,$dif,0 ,$oy,$oy);
								
								
								$nx = imagecreatetruecolor(300, 300);
								imagecopyresized($nx, $nm, 0,0,0,0,300,300,$oy,$oy);
								
								imagejpeg($nx, $path.'firststep/'. $filename);
								} 
								if ($oy > $ox){
								$di = ($oy - $ox);
								$dif = $di/2;
								
								$nm = imagecreatetruecolor($ox, $ox);
								imagecopy($nm, $im, 0,0,0,$dif ,$ox,$ox);
								
								
								$nx = imagecreatetruecolor(300, 300);
								imagecopyresized($nx, $nm, 0,0,0,0,300,300,$ox,$ox);
								
								imagejpeg($nx, $path.'firststep/'. $filename);
								}  
								if ($oy == $ox) {
								
								$nm = imagecreatetruecolor($ox, $oy);
								imagecopy($nm, $im, 0,0,0,0 ,$ox,$oy);
								
								
								$nx = imagecreatetruecolor(300, 300);
								imagecopyresized($nx, $nm, 0,0,0,0,300,300,$ox,$ox);
								
								imagejpeg($nx, $path.'firststep/'. $filename);
								
								}
					}			else {
					
					copy($path . $filename, $path . 'firststep/' . $filename);
					
					}
					
					
			 }
	
				 return json_encode($filename);
				 
			} else {
			
				return 'Unfourtenly we havent file';
			
			}	 
		
	} 
	


	public function get_remove()
	{
		User::current()->fill(array('deleted'=>1))->save();
		return Redirect::to('team');
	}


	public function get_changefoto($user_id){
	
		return  View::make('team.changefoto')->nest('mainheader','layouts.mainfooter',array('classh1'=>'Team','textinh1'=>'Notifications'))->with('user_id',$user_id);
	
	}
	
	
	public function post_savephotochanges(){
		
				$x1 = Input::get('x1');
				$y1 = Input::get('y1');
				$x2 = Input::get('x2');
				$y2 = Input::get('y2');
				$w = Input::get('w');
				$h = Input::get('h');
				$filename = Input::get('filename');
				$user_id = Input::get('user_id');
		
					$path = path('app').'../../upload/avatars/firststep/';
					
		
					if(preg_match('/[.](jpg)$/', $filename)) {
					$im = imagecreatefromjpeg($path.$filename);
					} else if (preg_match('/[.](gif)$/', $filename)) {
					$im = imagecreatefromgif($path.$filename);
					} else if (preg_match('/[.](png)$/', $filename)) {
					$im = imagecreatefrompng($path.$filename);
					} else if (preg_match('/[.](jpeg)$/', $filename)) {
					$im = imagecreatefromjpeg($path.$filename);
					}
					
					
				$nm = imagecreatetruecolor(100, 100);
				imagecopyresized($nm, $im, 0,0,$x1,$y1,100,100,$w,$h);
				imagejpeg($nm, $path.'../thumbs/'. $filename);

				
					if($filename != NULL){ 
						if($user_id != NULL){
							$cur_user = User::find($user_id);
							if(isset($cur_user->id)){
							
								$cur_user->avatar_name = $filename;
								$cur_user->save();
								
							}
						}
					
					}  
					
		return Redirect::to('team');		
		
	}
	
}